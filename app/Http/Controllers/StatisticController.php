<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatisticRequest;
use App\Models\Statistic;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Response
    {
        return Inertia::render('Statistic/Index', [
            'statistics' => Statistic::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        return Inertia::render('Statistic/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StatisticRequest $request): RedirectResponse
    {
        $statistic = Statistic::create($request->getData());

        return to_route('statistic.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Statistic $statistic): Response
    {
        return Inertia::render('Statistic/Edit', ['statistic' => $statistic]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StatisticRequest $request, Statistic $statistic): RedirectResponse
    {
        $statistic->name = $request->getData()['name'];
        $statistic->save();

        return to_route('statistic.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Statistic $statistic): RedirectResponse
    {
        $statistic->delete();

        return to_route('statistic.index');
    }
}
