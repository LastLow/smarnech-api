<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('action_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('magic_type_id')->nullable();
            $table->unsignedBigInteger('attack_type_id')->nullable();
            $table->unsignedBigInteger('guard_type_id')->nullable();

            $table->foreign('magic_type_id')
                ->references('id')
                ->on('magic_types')
                ->onDelete('cascade');
            $table->foreign('attack_type_id')
                ->references('id')
                ->on('attack_types')
                ->onDelete('cascade');
            $table->foreign('guard_type_id')
                ->references('id')
                ->on('guard_types')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('action_types');
    }
};
