<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('specializations', function (Blueprint $table) {
            $table->id();
            $table->double('value');
            $table->unsignedBigInteger('statistic_id');
            $table->unsignedBigInteger('action_type_id');
            $table->timestamps();

            $table->foreign('statistic_id')
                ->references('id')
                ->on('statistics')
                ->onDelete('cascade');
            $table->foreign('action_type_id')
                ->references('id')
                ->on('action_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('specializations');
    }
};
