<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('armaments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('model_id');
            $table->unsignedBigInteger('action_type_id');
            $table->unsignedBigInteger('rarity_id');
            $table->timestamps();

            $table->foreign('rarity_id')
                ->references('id')
                ->on('rarities')
                ->onDelete('cascade');

            $table->foreign('action_type_id')
                ->references('id')
                ->on('action_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('armaments');
    }
};
