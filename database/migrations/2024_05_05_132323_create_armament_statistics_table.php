<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('armament_statistics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('armament_id');
            $table->unsignedBigInteger('statistic_id');
            $table->double('value')->nullable();
            $table->enum('type', ['plus', 'multiply'])->default('plus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('armament_statistics');
    }
};
