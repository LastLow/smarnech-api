<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('combat_units', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('model_id');
            $table->unsignedBigInteger('image_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('rarity_id');
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')
                ->on('combat_unit_types')
                ->onDelete('cascade');
            $table->foreign('rarity_id')
                ->references('id')
                ->on('rarities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('combat_units');
    }
};
