<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('combat_unit_statistics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('combat_unit_id');
            $table->unsignedBigInteger('statistic_id');
            $table->double('value')->nullable();
            $table->timestamps();

            $table->foreign('combat_unit_id')
                ->references('id')
                ->on('combat_units')
                ->onDelete('cascade');
            $table->foreign('statistic_id')
                ->references('id')
                ->on('statistics')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('combat_unit_statistics');
    }
};
