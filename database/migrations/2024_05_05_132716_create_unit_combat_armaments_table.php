<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unit_combat_armaments', function (Blueprint $table) {
            $table->unsignedBigInteger('user_combat_unit_id');
            $table->unsignedBigInteger('user_armament_id');

            $table->unsignedBigInteger('type_id');

            $table->primary(['user_combat_unit_id', 'user_armament_id']);
            $table->timestamps();

            $table->foreign('user_combat_unit_id')
                ->references('id')
                ->on('user_combat_units')
                ->onDelete('cascade');
            $table->foreign('user_armament_id')
                ->references('id')
                ->on('user_armaments')
                ->onDelete('cascade');
            $table->foreign('type_id')
                ->references('id')
                ->on('unit_combat_armament_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unit_combat_armaments');
    }
};
