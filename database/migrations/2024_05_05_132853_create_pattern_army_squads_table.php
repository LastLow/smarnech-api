<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pattern_army_squads', function (Blueprint $table) {
            $table->unsignedBigInteger('army_id');
            $table->unsignedBigInteger('squad_id');
            $table->unsignedBigInteger('type_id');
            $table->primary(['army_id', 'squad_id']);
            $table->timestamps();

            $table->foreign('army_id')
                ->references('id')
                ->on('pattern_armies')
                ->onDelete('cascade');
            $table->foreign('squad_id')
                ->references('id')
                ->on('pattern_squads')
                ->onDelete('cascade');

            $table->foreign('type_id')
                ->references('id')
                ->on('squad_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pattern_army_squads');
    }
};
