<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pattern_squad_armaments', function (Blueprint $table) {
            $table->unsignedBigInteger('squad_id');
            $table->unsignedBigInteger('user_armament_id');
            $table->unsignedBigInteger('type_id');
            $table->primary(['squad_id', 'user_armament_id']);
            $table->timestamps();

            $table->foreign('squad_id')
                ->references('id')
                ->on('pattern_squads')
                ->onDelete('cascade');
            $table->foreign('user_armament_id')
                ->references('id')
                ->on('user_armaments')
                ->onDelete('cascade');


            $table->foreign('type_id')
                ->references('id')
                ->on('pattern_squad_armament_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pattern_squad_armaments');
    }
};
